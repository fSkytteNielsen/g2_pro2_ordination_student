package ordination;

import java.time.LocalDate;
import java.time.LocalTime;

public class DagligFast extends Ordination {
	private final Dosis[] dosisliste = new Dosis[4];
	private int dosisCount = 0;

	public DagligFast(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, Patient patient) {
		super(startDen, slutDen, laegemiddel, patient);

	}

	@Override
	public double samletDosis() {
		double dosisTotal = 0.0;
		for (Dosis dosis : dosisliste) {
			dosisTotal += dosis.getAntal();
		}
		return dosisTotal * antalDage();
	}

	@Override
	public double doegnDosis() {
		return samletDosis() / antalDage();
	}

	@Override
	public String getType() {
		String type = "Fast";
		return type;
	}

	// Associering metoder
	// ---------------------------------------------------------------------------------------
	public Dosis[] getDoser() {
		return dosisliste;
	}

	public Dosis createDosis(LocalTime tid, double antal) {
		Dosis dosis = new Dosis(tid, antal);
		if (dosisCount < 4) {
			dosisliste[dosisCount] = dosis;
			dosisCount++;
		}
		return dosis;
	}
}
