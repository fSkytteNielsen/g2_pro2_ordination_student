package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class DagligSkaev extends Ordination {
	// TODO

	private final List<Dosis> dosisList = new ArrayList<>();

	public DagligSkaev(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, Patient patient) {
		super(startDen, slutDen, laegemiddel, patient);
	}

	@Override
	public double samletDosis() {
		double dosisTotal = 0.0;
		for (Dosis dosis : dosisList) {
			dosisTotal += dosis.getAntal();
		}
		return dosisTotal * antalDage();
	}

	@Override
	public double doegnDosis() {
		return samletDosis() / antalDage();
	}

	@Override
	public String getType() {
		String type = "Skæv";
		return type;
	}

	// Associering metoder
	// ---------------------------------------------------------------------------------------
	public List<Dosis> getDoser() {
		return new ArrayList<>(dosisList);
	}

	public void createDosis(LocalTime tid, double antal) {
		Dosis dosis = new Dosis(tid, antal);
		if (!dosisList.contains(dosis)) {
			dosisList.add(dosis);
		}
	}
}
