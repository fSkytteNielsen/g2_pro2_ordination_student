package ordination;

import java.time.LocalDate;
import java.util.ArrayList;

public class PN extends Ordination {

	private double antalEnheder;

	private ArrayList<LocalDate> givetPaaDato = new ArrayList<>();

	public PN(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, Patient patient, double antalEnheder) {
		super(startDen, slutDen, laegemiddel, patient);
		this.antalEnheder = antalEnheder;
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true hvis
	 * givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
	 * Retrurner false ellers og datoen givesDen ignoreres
	 * 
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		boolean gyldig = false;
		if (givesDen.isAfter(this.getStartDen().minusDays(1)) && givesDen.isBefore(this.getSlutDen().plusDays(1))) {
			gyldig = true;
			givetPaaDato.add(givesDen);
		}

		return gyldig;
	}

	public double doegnDosis() {
		double dosis;
		if (givetPaaDato.isEmpty()) {
			dosis = 0.0;
		} else {
			dosis = (antalEnheder * givetPaaDato.size()) / antalDage();
		}
		return dosis;
	}

	public double samletDosis() {
		return antalEnheder * givetPaaDato.size();
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * 
	 * @return
	 */
	public int getAntalGangeGivet() {
		return givetPaaDato.size();
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	@Override
	public String getType() {
		String type = "PN";
		return type;
	}

}
