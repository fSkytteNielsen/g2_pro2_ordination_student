package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import controller.Controller;
import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.Ordination;
import ordination.PN;
import ordination.Patient;

public class ControllerTest {
	Controller controller;
	LocalDate dato1;
	LocalDate dato2;
	Patient p1;
	Laegemiddel l1;

	@Before
	public void setUp() throws Exception {
		controller = Controller.getTestController();
		p1 = controller.opretPatient("090149-2529", "Ib Hansen", 87.7);
		l1 = controller.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		dato1 = LocalDate.of(2021, 03, 01);
		dato2 = LocalDate.of(2021, 03, 02);
	}

	@Test
	public void testopretPNOrdination() {
		// arrange
		double antal = 1;
		// act
		PN ordination1 = controller.opretPNOrdination(dato1, dato2, p1, l1, antal);
		PN ordination2 = controller.opretPNOrdination(dato2, dato2, p1, l1, antal);
		// assert
		assertNotNull(ordination1);
		assertNotNull(ordination2);
		assertEquals(1, ordination1.getAntalEnheder(), 0.1);
		assertEquals(dato1, ordination1.getStartDen());
		assertEquals(dato2, ordination1.getSlutDen());
		assertEquals(dato2, ordination2.getStartDen());
		assertEquals(dato2, ordination2.getSlutDen());
		assertTrue(p1.getOrdinationer().contains(ordination1));
		assertEquals(ordination1.getLaegemiddel(), l1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testopretPNOrdinationException() {
		double antal = 1;
		// indsætter en startdato som er efter slutdato og forventer exception
		Ordination o1 = controller.opretPNOrdination(dato2, dato1, p1, l1, antal);
	}

	@Test
	public void testopretDagligFastOrdination() {
		// Arrange
		double morgenAntal = 0;
		double middagAntal = 0;
		double aftenAntal = 0;
		double natAntal = 1;
		DagligFast ordination1 = controller.opretDagligFastOrdination(dato1, dato2, p1, l1, morgenAntal, middagAntal,
				aftenAntal, natAntal);
		assertNotNull(ordination1);
		assertEquals(dato1, ordination1.getStartDen());
		assertEquals(dato2, ordination1.getSlutDen());
		assertTrue(p1.getOrdinationer().contains(ordination1));
		assertEquals(ordination1.getLaegemiddel(), l1);

		assertEquals(ordination1.getDoser()[0].getAntal(), 0, 0.1);
		assertEquals(ordination1.getDoser()[1].getAntal(), 0, 0.1);
		assertEquals(ordination1.getDoser()[2].getAntal(), 0, 0.1);
		assertEquals(ordination1.getDoser()[3].getAntal(), 1.0, 0.1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testopretDagligFastOrdinationException() {
		double morgenAntal = 0;
		double middagAntal = 0;
		double aftenAntal = 0;
		double natAntal = 1;
		// indsætter en startdato som er efter slutdato og forventer exception
		DagligFast ordination1 = controller.opretDagligFastOrdination(dato2, dato1, p1, l1, morgenAntal, middagAntal,
				aftenAntal, natAntal);
	}

	@Test
	public void testOpretDagligSkaevOrdination() {
		double[] antalEnheder = { 1, 2 };
		LocalTime[] klokkeslæt = { LocalTime.of(10, 00), LocalTime.of(14, 00) };
		DagligSkaev ordination1 = controller.opretDagligSkaevOrdination(dato1, dato2, p1, l1, klokkeslæt, antalEnheder);
		assertNotNull(ordination1);
		assertEquals(dato1, ordination1.getStartDen());
		assertEquals(dato2, ordination1.getSlutDen());
		assertTrue(p1.getOrdinationer().contains(ordination1));
		assertEquals(ordination1.getLaegemiddel(), l1);

		assertEquals(ordination1.getDoser().size(), antalEnheder.length);
		assertEquals(ordination1.getDoser().size(), klokkeslæt.length);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOpretDagligSkaevOrdinationSlutFoerStart() {
		double[] antalEnheder = { 1, 2 };
		LocalTime[] klokkeslæt = { LocalTime.of(10, 00), LocalTime.of(14, 00) };
		// Sætter bevidst en slutdato ind foer startdato
		DagligSkaev ordination1 = controller.opretDagligSkaevOrdination(dato2, dato1, p1, l1, klokkeslæt, antalEnheder);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOpretDagligSkaevOrdinationFoskelligLaengdeArray() {
		// Indsætter ekstra antal på antalEnheder
		double[] antalEnheder = { 1, 2, 3 };
		LocalTime[] klokkeslæt = { LocalTime.of(10, 00), LocalTime.of(14, 00) };
		DagligSkaev ordination1 = controller.opretDagligSkaevOrdination(dato1, dato2, p1, l1, klokkeslæt, antalEnheder);
	}

	@Test
	public void testordinationPNAnvendt() {
		double antal = 1;
		PN ordination1 = controller.opretPNOrdination(dato1, dato2, p1, l1, antal);
		controller.ordinationPNAnvendt(ordination1, dato1);
		assertEquals(1, ordination1.getAntalGangeGivet());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testordinationPNAnvendtUdenforGyldighed() {
		double antal = 1;
		PN ordination1 = controller.opretPNOrdination(dato1, dato2, p1, l1, antal);
		// Indsætter dato der er udenfor gyldighedsperioden
		controller.ordinationPNAnvendt(ordination1, dato1.minusDays(1));
	}

	@Test
	public void testanbefaletDosisPrDoegn() {
		Patient p1 = controller.opretPatient("090149-2529", "Ib Hansen", 24);
		Patient p2 = controller.opretPatient("090149-2529", "Ib Hansen", 80);
		Patient p3 = controller.opretPatient("090149-2529", "Ib Hansen", 121);
		assertEquals(24, controller.anbefaletDosisPrDoegn(p1, l1), 0.1);
		assertEquals(120, controller.anbefaletDosisPrDoegn(p2, l1), 0.1);
		assertEquals(242, controller.anbefaletDosisPrDoegn(p3, l1), 0.1);
	}

	@Test
	public void testantalOrdinationerPrVægtPrLægemiddel() {
		Ordination o1 = controller.opretPNOrdination(dato1, dato2, p1, l1, 1);
		p1.addOrdination(o1);
		int antalOrdinationer = controller.antalOrdinationerPrVægtPrLægemiddel(20, 100, l1);
		int antalOrdinationer2 = controller.antalOrdinationerPrVægtPrLægemiddel(100, 120, l1);
		assertEquals(1, antalOrdinationer);
		assertEquals(0, antalOrdinationer2);
	}
}
