package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.Laegemiddel;
import ordination.Patient;

public class DagligFastTest {
	DagligFast df;
	Patient p1;
	Laegemiddel l1;

	@Before
	public void setUp() throws Exception {
		p1 = new Patient("090149-2529", "Ib Hansen", 87.7);
		l1 = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		df = new DagligFast(LocalDate.of(2021, 3, 1), LocalDate.of(2021, 3, 2), l1, p1);
	}

	@Test
	public void testconstructorDaglisSkaev() {
		assertNotNull(df);
		assertEquals(LocalDate.of(2021, 3, 1), df.getStartDen());
		assertEquals(LocalDate.of(2021, 3, 2), df.getSlutDen());
		assertEquals(l1, df.getLaegemiddel());
		assertTrue(p1.getOrdinationer().contains(df));
	}

	@Test
	public void testsamletDosis() {
		df.createDosis(LocalTime.of(9, 00), 0);
		df.createDosis(LocalTime.of(12, 00), 0);
		df.createDosis(LocalTime.of(18, 00), 0);
		df.createDosis(LocalTime.of(23, 59), 1);
		assertEquals(4, df.getDoser().length);
	}

	@Test
	public void testdoegnDosis() {
		df = new DagligFast(LocalDate.of(2021, 3, 1), LocalDate.of(2021, 3, 1), l1, p1);
		df.createDosis(LocalTime.of(9, 00), 0);
		df.createDosis(LocalTime.of(12, 00), 0);
		df.createDosis(LocalTime.of(18, 00), 0);
		df.createDosis(LocalTime.of(23, 59), 1);
		assertEquals(1, df.doegnDosis(), 0.001);
	}

}
