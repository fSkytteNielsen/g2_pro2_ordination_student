package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.Patient;

public class DagligSkaevTest {
	DagligSkaev ds;
	Patient p1;
	Laegemiddel l1;

	@Before
	public void setUp() throws Exception {
		p1 = new Patient("090149-2529", "Ib Hansen", 87.7);
		l1 = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		ds = new DagligSkaev(LocalDate.of(2021, 3, 1), LocalDate.of(2021, 3, 2), l1, p1);
	}

	@Test
	public void testconstructorDaglisSkaev() {
		assertNotNull(ds);
		assertEquals(LocalDate.of(2021, 3, 1), ds.getStartDen());
		assertEquals(LocalDate.of(2021, 3, 2), ds.getSlutDen());
		assertEquals(l1, ds.getLaegemiddel());
		assertTrue(p1.getOrdinationer().contains(ds));
	}

	@Test
	public void testsamletdosis() {
		ds.createDosis(LocalTime.of(10, 00), 4);
		ds.createDosis(LocalTime.of(10, 01), 5);
		assertEquals(2, ds.getDoser().size());
		assertEquals(ds.samletDosis(), (5 + 4) * ds.antalDage(), 0.1);
	}

}
