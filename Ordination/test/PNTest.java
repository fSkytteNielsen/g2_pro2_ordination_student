package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

public class PNTest {
	PN pn;
	Patient p1;
	Laegemiddel l1;

	@Before
	public void setUp() throws Exception {
		int antalEnheder = 1;
		p1 = new Patient("090149-2529", "Ib Hansen", 87.7);
		l1 = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		pn = new PN(LocalDate.of(2021, 3, 1), LocalDate.of(2021, 3, 2), l1, p1, antalEnheder);
	}

	@Test
	public void testgivDosis() {
		pn.givDosis(LocalDate.of(2021, 3, 2));
		assertEquals(pn.getAntalGangeGivet(), 1);
		// tjek om dag udenfor ordingernsperioden
		assertFalse(pn.givDosis(LocalDate.of(2021, 3, 3)));
	}

	@Test
	public void testDoegnDosis() {
		pn.givDosis(LocalDate.of(2021, 3, 1));
		pn.givDosis(LocalDate.of(2021, 3, 2));
		assertEquals(pn.doegnDosis(), 1, 0.1);
	}

	@Test
	public void testSamletDosis() {
		pn.givDosis(LocalDate.of(2021, 3, 1));
		pn.givDosis(LocalDate.of(2021, 3, 2));
		assertEquals(pn.samletDosis(), 2, 0.1);
	}

}
